<?php
session_start();
include_once('../../vendor/autoload.php');
use App\Hobbies\Hobbies;







?>

<!DOCTYPE html>

<head>
    <title>ATOMIC PROJECTS - INDEX PAGE</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../resource/bootstrap/js/jquery.min.js"></script>

    <style>
        body{height:100%;
            width:100%;
            background-image:url(../../resource/img/bg.jpeg);/*your background image*/
            background-repeat:no-repeat;/*we want to have one single image not a repeated one*/
            background-size:cover;/*this sets the image to fullscreen covering the whole screen*/
            /*css hack for ie*/
            /*filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='.image.jpg',sizingMethod='scale');*/
            /*-ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='image.jpg',sizingMethod='scale')";*/
        }
    </style>
</head>

<body class="body">
<div class="container" ">
    <h2 style="color: #eeeeee">ATOMIC PROJECT - INDEX</h2>


    <table>
        <tr>
            <td height="100">


                <div id="AtomicProjectListMenu">
                    <button type="button" onclick="window.location.href='Booktitle/create.php' " class=" btn-success btn-lg">Book Title</button>
                    <button type="button" onclick="window.location.href='Birthdate/create.php'" class=" btn-primary btn-lg"><a href='Birthdate/create.php'></a>Birthday</button>

                    <button type="button" onclick="window.location.href='CityName/create.php'" class=" btn-danger btn-lg"><a href='CityName/create.php'></a>City</button>
                    <button type="button" onclick="window.location.href='Email/create.php'" class=" btn-primary btn-lg"><a href='Email/create.php'></a>Email Subscription</button>
                    <button type="button" onclick="window.location.href='Gender/create.php'" class=" btn-success btn-lg"><a href='Gender/create.php'></a>Gender</button>
                    <button type="button" onclick="window.location.href='Hobbies/create.php'" class=" btn-danger btn-lg"><a href='Hobbies/create.php'></a>Hobby</button>
                    <button type="button" onclick="window.location.href='ProfilePicture/create.php'" class=" btn-primary btn-lg"><a href='ProfilePicture/create.php'></a>Profile Picture</button>
                    <button type="button" onclick="window.location.href='SummaryOfOrganization/create.php'" class=" btn-success btn-lg"><a href='SummaryOfOrganization/create.php'></a>Summary Of Organization</button>




                </div>
            </td>

        </tr>
    </table>




</body>

<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>




<script>
    function ConfirmDelete(id)
    {
        var x = confirm("Are you sure you want to delete ID# "+id+" ?");
        if (x)
            return true;
        else
            return false;
    }




    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    $(document).ready(function() {
        $("#checkall").click(function() {
            var checkBoxes = $("input[name=mark\\[\\]]");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        });
    });





</script>


</HTML>

