<?php
namespace App\CityName;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO ;

class CityName extends DB{
    public $id="";
    public $person_name="";
    public $city_name="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('person_name',$postVariableData)){
            $this->person_name = $postVariableData['person_name'];
        }

        if(array_key_exists('city_name',$postVariableData)){
            $this->city_name = $postVariableData['city_name'];
        }
    }



    public function store(){

    $arrData = array( $this->person_name, $this->city_name);

    $sql = "Insert INTO city(person_name, city_name) VALUES (?,?)";
    $STH = $this->DBH->prepare($sql);

    $result = $STH->execute($arrData);

     if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
     else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method



    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from city where is_deleted = 'no' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();






    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from city where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function update(){

        $arrData = array ($this->person_name, $this->city_name);
        $sql = "UPDATE city SET person_name = ?, city_name = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()


    public function delete(){

        $sql = "Delete from city where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "Update city SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result=$STH->execute();
        if($result)
            Message::message("Success! Data Has Been trashed");
        else
            Message::message("Failed! Data Has Not Been trashed");


        Utility::redirect('index.php');


    }// end of trash()

    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from city where is_deleted <>'no' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();


public function recover(){
    $sql='UPDATE city  SET is_deleted  = "no" where id ='.$this->id;

    $STH=$this->DBH->prepare($sql);
    $result=$STH->execute();
    if($result)
        Message::message(" Data Has Been recovered ");


    else
        Message::message("Data Hasn't Been recovered ");
    Utility::redirect('index.php');




}
}//end of CityName Class

?>

