-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2016 at 09:30 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `person_name` varchar(100) NOT NULL,
  `birthdate` date NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `person_name`, `birthdate`, `is_deleted`) VALUES
(1, 'Mohammad', '1991-04-19', 'no'),
(2, 'Younus', '1993-02-01', 'no'),
(4, 'jjjyiyiio', '2016-11-21', '2016-11-21 00:44:48');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(100) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_deleted`) VALUES
(3, 'moshah', 'mishu', 'No'),
(4, 'maddad', 'dfdh', 'no'),
(5, 'ggjdhddd', 'fgfdggdf', '2016-11-21 00:33:33'),
(6, 'dffsd', 'gjh', 'no'),
(7, 'ig', 'kk', 'No'),
(8, 'fhlfhhgaaf', 'fsgfhgj', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `person_name` varchar(100) NOT NULL,
  `city_name` varchar(50) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `person_name`, `city_name`, `is_deleted`) VALUES
(1, 'Rahim', 'Chittagong', 'no'),
(2, 'Korim', 'Dhaka', 'no'),
(3, 'kkkkk', 'Chittagong', 'no'),
(4, 'iiii', 'Chittagong', '2016-11-21 00:53:02');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `person_name` varchar(100) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `person_name`, `email_id`, `is_deleted`) VALUES
(1, 'Masud', 'masud.chy@ymail.com', 'no'),
(2, 'Iqbal', 'i12@gmail.com', 'no'),
(3, 'fffffffffffff', 'k@g.com', 'no'),
(4, 'ey', 'dsdf@fgh.com', '2016-11-21 00:58:59');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `person_name` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `is_deleted` int(111) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `person_name`, `gender`, `is_deleted`) VALUES
(1, 'Rohim', 'Male', 0),
(2, 'Nishat', 'Female', 0),
(3, 'jkgjsj', 'Female', 0),
(4, 'jlj', 'Other', 0),
(5, 'jilu', 'other', 2147483647),
(6, 'affea', 'Female', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `person_name` varchar(50) NOT NULL,
  `hobbies` varchar(100) NOT NULL,
  `is_deleted` int(111) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `person_name`, `hobbies`, `is_deleted`) VALUES
(1, 'Fahmida', 'Gardening', 0),
(2, 'Kobir', 'Traveling', 0),
(3, 'jmhm', 'Drawing,Gardening', 0),
(4, 'hj,,h,', 'Gardening', 0),
(5, 'hj', 'Cycling,Photography,Programming', 2147483647),
(6, 'hghsdglh', 'Drawing,Programming', 0);

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `person_name` varchar(50) NOT NULL,
  `profile_picture` varchar(100) NOT NULL,
  `is_deleted` int(111) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `person_name`, `profile_picture`, `is_deleted`) VALUES
(3, 'mmm', 'images (2).jpg', 2147483647),
(4, 'tt', '1479709661unnamed.jpg', 2147483647),
(5, 'rggrege', '1479709996images (1).jpg', 0),
(6, 'seefs', '1479715046nature.jpg', 2147483647),
(7, 'ksjgjgjw', '14797155124-landscape painting by madart.jpg', 0),
(8, 'sudhsdif', '147971552417-3d-pencil-drawing.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(11) NOT NULL,
  `organization_name` varchar(50) NOT NULL,
  `organization_summary` varchar(100) NOT NULL,
  `is_deleted` int(111) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `organization_name`, `organization_summary`, `is_deleted`) VALUES
(1, 'Cooper''s', 'Cooper''s is a bakery chain in Bangladesh. It was founded in 1984 by Douglas Cooper.', 0),
(2, 'Aarong', 'Aarong is one of the retail chains in Bangladesh operating under BRAC, a non-profit NGO. ', 0),
(3, 'hkhkh', 'lflv\r\nddvdb', 2147483647),
(4, 'rehreh', 'etuetut', 0),
(5, 'sh', 'gsgs', 0),
(6, 'dsg', 'ghghh\r\nghfgh', 2147483647);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
